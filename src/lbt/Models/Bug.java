/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lbt.Models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jesusgallego
 */
public class Bug {
    private int bugType;
    private String name;
    private String description;
    private User user;
    private List<Comment> comments = new ArrayList<>();

    /**
     * @return the bugType
     */
    public String getBugType() {
        
        String t = "";
        
        switch (bugType) {
            case 0 :
                t = "Important";
                break;
            case 1 :
                t = "Normal";
                break;
            case 2 :
                t = "Minor";
                break;
            case 3 :
                t = "Wishlist";
                break;
        }
        
        return t;
    }

    /**
     * @param bugType the bugType to set
     */
    public void setBugType(int bugType) {
        this.bugType = bugType;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    
     /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }
    
    public void newComment(Comment c) {
        comments.add(c);
    }

    @Override
    public String toString() {
        String s = "\nDescripción del Bug: \n";
        s += "\tNombre: " + getName() + "\n";
        s += "\tDescripción: " + getDescription()+ "\n";
        s += "\tTipo: " + getBugType()+ "\n";
        s += "\tUsuario: [" + getUser().toString() + "]\n";
        
        return s;
    }


    
    
    
}
