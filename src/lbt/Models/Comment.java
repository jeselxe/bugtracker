/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lbt.Models;

/**
 *
 * @author jesusgallego
 */
public class Comment {
    private User user;
    private String comment;

    public Comment(User u, String c) {
        user = u;
        comment = c;
    }
    
    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return comment + " (" + user.getName() + ")"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
