/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lbt.Controllers;

import java.util.List;
import lbt.Models.*;

/**
 *
 * @author jesusgallego
 */
public class BugController {
    
    public void newBug(User user, List<Bug> bugs, int tipo, String name, String description) {
        
        Bug bug = new Bug();
        bug.setUser(user);
        bug.setBugType(tipo);
        bug.setName(name);
        bug.setDescription(description);
        
        bugs.add(bug);
        System.out.println(bug.toString());
        
    }
    
    public void newComment(Bug b, User u, String c) {
        b.newComment(new Comment(u,c));
    }
    
}
